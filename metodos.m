function varargout = metodos(varargin)
% METODOS MATLAB code for metodos.fig
%      METODOS, by itself, creates a new METODOS or raises the existing
%      singleton*.
%
%      H = METODOS returns the handle to a new METODOS or the handle to
%      the existing singleton*.
%
%      METODOS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in METODOS.M with the given input arguments.
%
%      METODOS('Property','Value',...) creates a new METODOS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before metodos_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to metodos_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help metodos

% Last Modified by GUIDE v2.5 04-Apr-2019 17:22:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @metodos_OpeningFcn, ...
                   'gui_OutputFcn',  @metodos_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before metodos is made visible.
function metodos_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to metodos (see VARARGIN)

% Choose default command line output for metodos
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes metodos wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = metodos_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btncalcular.

% hObject    handle to btncalcular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function [output] =  invalido(nu)
 if (isempty(nu) | ~isfinite(nu) | (nu==-999) )
             output=true;
 else
            output=false;
 end


% --- Executes on button press in btncalcular.
function btncalcular_Callback(hObject, eventdata, handles)
%       syms = Crea variables simbolicas.
%       sym  = Convierte a variable simbo�lica.
%    SECANTE
        % a�adimos los columnName
        set(handles.tabla,'ColumnName',{'i','x(n-1)','x(n)','x(n+1)','error'});
        % capturamos los datos del formulario
        f=get(handles.edit1,'string');
        a=str2double(get(handles.edit2,'string'));%
        b=str2double(get(handles.edit3,'string'));
        tolerancia=str2double(get(handles.edit4,'string'));
        %limpiamos la tabla y el grafico del formulario
        set(handles.tabla,'Data',[]);
        cla reset;
        % realizamos la validacion correspondiente
        if(invalido(a) || invalido(b) || invalido(tolerancia))       
         set(handles.respuesta,'string',"datos introducidos invalidos");        
         return;
        end
        syms x;
        ea(1)=100;
        hold off
        cla
        set(handles.respuesta,'string','Funcion Incorrecta');
        i=1;
        % Se realiza el ciclo donde se calcula los puntos siguiendo
        %la formula de la secante y se registra cada iteraci�n en
        %la tabla hasta encontrar y mostrar la ra�z.
        while abs(ea)>tolerancia;
        x=a;
        a00=a;
        fxa=eval(f);
        x=b;
        fxb=eval(f);
        %formula de la secante
        xi=a-((fxa*(b-a))/(fxb-fxa));
        ea=abs((xi-b)/xi)*100;
        a=b;
        b=xi;
        %cargamos los datos de la iteracion en la tabla
            newRow ={i,a00,x xi,ea};
            oldData = get(handles.tabla,'Data');
            newData=[oldData; newRow];
            set(handles.tabla,'Data',newData)
        i=i+1;
        end
        %respuesta encontrada 
        %mostramos la respuesta en el formulario
        respuesta=sprintf('%0.20f',x);
        set(handles.respuesta,'string',respuesta);

        %mostramos el grafico de la funcion
        
        hold off
         hold on;
        zoom on;

        fplot(handles.axes,f);
        fplot(0,[x x],'or')
        hold off
        grid on
        title(strcat('Funcion: ',f));      
        xlabel(' eje x');
        ylabel('eje y');

        show();
        
        



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function repuesta_Callback(hObject, eventdata, handles)
% hObject    handle to repuesta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of repuesta as text
%        str2double(get(hObject,'String')) returns contents of repuesta as a double


% --- Executes during object creation, after setting all properties.
function repuesta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to repuesta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function respuesta_Callback(hObject, eventdata, handles)
% hObject    handle to respuesta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of respuesta as text
%        str2double(get(hObject,'String')) returns contents of respuesta as a double


% --- Executes during object creation, after setting all properties.
function respuesta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to respuesta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnFalsaPosicion.
function btnFalsaPosicion_Callback(hObject, eventdata, handles)
%function falsapos(fx,xl,xu,es)

   % a�adimos los columnName
    set(handles.tabla,'ColumnName',{'i','a','x','b','error'});
    %capturamos los datos del formulario
    fx=get(handles.edit1,'string');
    a=str2double(get(handles.edit2,'string'));%
    b=str2double(get(handles.edit3,'string'));
    tolerancia=str2double(get(handles.edit4,'string'));
    %limpiamos la tabla y el formulario
    set(handles.tabla,'Data',[]);
    cla reset;
    %validamos los datos
    if(invalido(a) || invalido(b) || invalido(tolerancia))       
     set(handles.respuesta,'string',"datos introducidos invalidos");        
     return;
    end
    %inicializamos variable
    es=tolerancia;
    f=inline(fx);
    fl=f(a);
    fu=f(b);
    iu=0;
    il=0;
    xr=100;
    do=0;
    iter=0;
    %realizamos las iteraciones hasta encontrar la respuesta
while(do==0)
    xrold=xr;
    xr= b-((fu*(a-b))/(fl-fu));
    fr=f(xr);
    fll=a;
    if(xr~=0)
        ea=abs(((xr-xrold)/xr) * 100);
    end
    test=fl*fr;
    %verificamos si es negativo
    if(test<0)
        b=xr;
        fu=f(b);
        iu=0;
        il=il+1;
        if(il>=2)
          %dividimos entre dos
            fl=fl/2;
        end
    else
        %verificamos si es positivo
        if(test>0)
            a=xr;
            fl=f(a);
            il=0;
            iu=iu+1;
              if(iu>=2)
                 %dividimos entre dos
                  fu=fu/2;
              end
        else
            ea=0;
        end
         iter=iter+1;
          % a�adimos los datos de la iteracion a la tabla
         newRow ={iter,fll, xr,b,ea};
            oldData = get(handles.tabla,'Data');
            newData=[oldData; newRow];
            set(handles.tabla,'Data',newData)
        
        if((ea<es))
            break;
        end
    end
end

%mostramos el resultado en el formulario
respuesta=sprintf('%0.20f',xr);
        set(handles.respuesta,'string',respuesta);

disp(xr);
             
%mostramos el grafico de la funcion
 hold off
         hold on;
        zoom on;

        fplot(handles.axes,fx);
        fplot(0,[xr xr],'or')
        hold off
        grid on
        title(strcat('Funcion: ',fx));      
        xlabel(' eje x');
        ylabel('eje y');

        show();
        

      %  fplot('1',x,'+-');
     
             
             
             
             
             
